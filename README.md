# FFMPEG tricks

## Timespec

For seeking there 2 ways to write the duration : 
(See `man 1 ffmpeg-utils`)

- `[-][<HH>:]<MM>:<SS>[.<m>...]` :
	+ `-` is negative duration
	+ `HH` is hours
	+ `MM` is minutes
	+ `SS` is seconds
	+ `m` is milliseconds
- `[-]<S>+[.<m>...]` :
	+ `-` is negative duration
	+ `S` is seconds
	+ `m` is milliseconds

## Cutting sections

See : https://trac.ffmpeg.org/wiki/Seeking

Seek using `-ss TIMESPEC` like `-ss 00:23:00` which seeks to 23 minutes into the video.

Input seeking must be before `-i` and output after `-i`

Use `-t TIMESPEC` to set a maximum duration or `-to TIMESPEC` to set the end time to cut off

Examples :
- `-ss 00:23:00 -i INPUTFILE -t 10` : Cut a section that starts at 23 minutes and lasts 10 seconds
- `-ss 00:23:00 -i INPUTFILE -to  00:24:00` : Cut a section that starts at 23 minutes and lasts until the 24th minute. So the resulting clip will be 1 minute long.

## Accelerated decoding

See : https://trac.ffmpeg.org/wiki/HWAccelIntro

Some codecs can be decoded via GPU like VC1, H264, HEVC, VP8, VP9

See available hardware accels : `ffmpeg -hwaccels`

See available video encoders : `ffmpeg -encoders`

### Nvidia

#### NVDEC

`ffmpeg -hwaccel nvdec input output`

#### CUVID

`ffmpeg -hwaccel cuvid -c:v h264_cuvid -i input output`

### Intel

See : https://trac.ffmpeg.org/wiki/Hardware/QuickSync

Using quicksync with libmfx or vaapi

#### Quicksync

`ffmpeg -hwaccel qsv -c:v hevc_qsv -load_plugin hevc_hw -i input.mp4 -vf hwdownload,format=p010 -pix_fmt p010le output.yuv`

#### Vaapi

`ffmpeg -hwaccel vaapi -hwaccel_output_format vaapi -i input.mp4 -vf 'scale_vaapi=1280:720,hwmap=derive_device=qsv,format=qsv' -c:v h264_qsv out.mp4`

## List encoder options

`ffmpeg -h encoder=h264_nvenc`

## H264

Most widespread codec, even if HEVC is more efficient it might need to be recoded on some video streaming sites.

See : https://trac.ffmpeg.org/wiki/Encode/H.264

ffmpeg flag : `-c:v libx264`

### Preset

`placebo` is not worth it. Start from `veryslow`, `slower`, `slow`.

### Tune

ffmepg flag : `-tune`

- `film` : high quality movies or real life recordings
- `animation` : anime
- `grain` : old videos with speckles and grain
- `stillimage` : slideshows or content that doesn't change that much
- `fastdecode` : disables some filters that make decoding more intensive
- `zerolatency` : useful when streaming, not when encoding for submission

### Profile

ffmpeg flag : `-profile:v`

Ignore this or set one of :

- `baseline`
- `main`
- `high`
- `high10`
- `high422`
- `high444`

Generally choose `high` profile.

The highest compatibility with all devices is achieved with `-profile:v baseline -level 3.0`

### Muxer

Use `faststart` for web video encoding.

`-movflags +faststart`

### Two-pass

- Use `-pass 1` and then `-pass 2` options.
- Output in first pass to null

```
ffmpeg -y -i input -c:v libx264 -b:v 2600k -pass 1 -an -f mp4 /dev/null && \
ffmpeg -i input -c:v libx264 -b:v 2600k -pass 2 -c:a aac -b:a 128k output.mp4
```

### Pixel format

Add `-pix_fmt yuv420p` to output to force the correct pixel format.

### Additional options

- `-sc_threshold 0` : to disable scenecut
- `-g 60` : the GOP to use, usually twice the framerate
- `-r 30` : set the framerate
- `-bf 3` : amount of bframes to use, 3 is good for most players
- `-keyint_min 60` : linked with GOP, minimum GOP size
- `-bufsize 1216k` : should be video bitrate + audio bitrate (e.g: `-b:v 1024k -b:a 192k -bufsize 1216k`)
- `-maxrate 1280k` : target bitrate * 1.25

### Bitrates

Calc bitrate with `(bits/pixel x (width x height) x frame rate)/1000` in kbps

- 720p 30fps : 5Mbps
- 720p 60fps : 7.5Mbps
- 1080p 30fps : 8Mbps
- 1080p 60fps : 12Mbps

ffmpeg flag : `-b:v 8m`

### Complete 2-pass encoding lines

```
ffmpeg -hwaccel nvdec \
	-ss 00:01:00 -i INPUTFILE -t 60 \
	-pix_fmt yuv420p \
	-c:v libx264 -preset slow -tune animation -profile:v high \
	-b:v 12m -bufsize 12m -maxrate 15m \
	-r 60 -g 120 -keyint_min 120 -bf 3 -sc_threshold 0 \
	-pass 1 \
	-an -f mp4 -y /dev/null
ffmpeg -hwaccel nvdec \
	-ss 00:01:00 -i INPUTFILE -t 60 \
	-pix_fmt yuv420p \
	-c:v libx264 -preset slow -tune animation -profile:v high \
	-b:v 12m -bufsize 12m -maxrate 15m \
	-r 60 -g 120 -keyint_min 120 -bf 3 -sc_threshold 0 \
	-pass 2 \
	-an -f mp4 -movflags +faststart -y output.mp4
```

## Audio

Youtube recommends AAC-LC

Opus is the best choice to preserve as much as possible of the original audio.
But its inclusion inside an mp4 container is experimental.

ffmpeg flag : `-c:a libopus`

Better use aac : `-c:a aac`

Some general options to apply :

- `-af "aresample=async=1:min_hard_comp=0.100000:first_pts=0"` : lines up the audio with the beginning of the video
- `-ac 2` : forces output audio to stereo
- `-ar 48000` : force audio samplerate to 48KHz, this should be 48KHz for opus
- `-an` : disables audio
- `-sample_fmt s16` : force audio bit depth, see `ffmpeg -sample_fmts` for available formats

## Complete 2-pass encoding with audio

```
INPUTFILE="2019-02-20_14-39-09.ts"
ffmpeg -hwaccel auto \
	-ss 00:02:00 -i "${INPUTFILE}" -to 00:09:00 \
	-pix_fmt yuv420p \
	-c:v libx264 -preset medium -tune animation -profile:v high -x264opts opencl \
	-b:v 8000k -bufsize 8192k -maxrate 10000k \
	-r 60 -g 120 -bf 3 -sc_threshold 0 \
	-pass 1 \
	-an -f mp4 -y /dev/null && \
ffmpeg -hwaccel auto \
	-ss 00:02:00 -i "${INPUTFILE}" -to 00:09:00 \
	-pix_fmt yuv420p \
	-c:v libx264 -preset medium -tune animation -profile:v high -x264opts opencl \
	-b:v 8000k -bufsize 8192k -maxrate 10000k \
	-r 60 -g 120 -bf 3 -sc_threshold 0 \
	-pass 2 \
	-c:a aac -b:a 192k -ac 2 -ar 48000 \
	-af "aresample=async=1:min_hard_comp=0.100000:first_pts=0" \
	-f mp4 -movflags +faststart -y output.mp4
```

## OBS

### Streaming

#### Nvidia

|Key|Value|Comment|
|---|---|---|
|Encoder|NVIDIA NVENC H.264|Accelerated video encoding|
|Enforce streaming service encoder settings|[x]|Respect streaming service settings|
|Rescale Output|[ ]|If rescaling don't do it here|
|Rate Control|CBR|Constant Rate is better for streaming|
|Bitrate|12000|12000 should be ok for 1080p 60fps|
|Keyframe Interval|2|For 60fps, 2 represents a GOP of 120|
|Preset|Quality|The encoder is very fast|
|Profile|high|For best quality settings|
|GPU|0|The GPU to use for encoding|
|Max B-frames|2||

#### Intel

Using vaapi offloading encoding to the gpu.

|Key|Value|Comment|
|---|---|---|
|Encoder|FFMPEG VAAPI|Accelerated video encoding|
|Enforce streaming service encoder settings|[x]|Respect streaming service settings|
|Rescale Output|[ ]|If rescaling don't do it here|
|VAAPI Device|Card0|Choose the right dri device|
|VAAPI Codec|H.264|Not much choice here|
|Level|4.2|Set to at least 4.2|
|Bitrate|12000|12000 should be ok for 1080p 60fps|
|Keyframe Interval|2|For 60fps, 2 represents a GOP of 120|

### Recording

#### Nvidia

|Key|Value|Comment|
|---|---|---|
|Type|Custom output (FFmpeg)||
|Container Format|mpegts||
|Video Bitrate|80000||
|Keyframe Interval|120|For 60 fps|
|Rescale Output|[ ]|Don't do it here|
|Video Encoder|hevc_nvenc|hevc compresses better so more quality for the same size as h264|
|Video Encoder Settings|`-bf 2 -preset slow -rc vbr_hq -2pass 1 -rc-lookahead 120 -temporal-aq 1`||
|Audio Encoder|s302m|Uncompressed audio compatible with mpegts|

### Audio

|Key|Value|Comment|
|---|---|---|
|Sample Rate|48khz||

### Videos

|Key|Value|Comment|
|---|---|---|
|Downscale Filter|Lanczos|Even lanczos is very fast|
|Common FPS Values|60|Should be 60 on dedicated graphics card|

### Advanced

|Key|Value|Comment|
|---|---|---|
|Color Format|NV12|Never change this|
|YUV Color Space|709|709 is still worth for keeping all the colors|
|YUV Color Range|Full|Keep the whole color range|

## References

- https://videoblerg.wordpress.com/2017/11/10/ffmpeg-and-how-to-use-it-wrong/
- https://support.google.com/youtube/answer/1722171?hl=en
- https://www.wowza.com/docs/how-to-encode-source-video-for-wowza-streaming-cloud#vbitrate

