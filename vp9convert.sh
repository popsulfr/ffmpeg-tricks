#!/usr/bin/env bash
IFS=$'\n\t'
set -euo pipefail

if [[ "${#}" -lt 1 ]] ; then
	echo "Needs argument" 1>&2
	exit 1
fi

EXTENSION="webm"
INPUTFILE="$(readlink -f "${1:-}")"
OUTPUTFILE="$(readlink -f "$(basename "${INPUTFILE%.*}.${EXTENSION}")")"

SS=(-ss 00:02:00)
TO=(-to 00:01:00)
FFMPEG_DEF_FLAGS=(-hide_banner -nostdin -hwaccel auto)

FFMPEG_PASS_PREFIX="$(basename "${OUTPUTFILE}")"

FFMPEG_VIDEO_BITRATE="8000"
FFMPEG_VIDEO_FRAMERATE="60"
FFMPEG_AUDIO_BITRATE="128"

FFMPEG_ALL_PASS_FLAGS=(
	-pix_fmt yuv420p
	-c:v libvpx-vp9
	-auto-alt-ref 1
	-lag-in-frames 25
	-deadline good
	-cpu-used 2
	-tile-columns 6
	-frame-parallel 1
	-row-mt 1
	-threads 0
	-b:v "${FFMPEG_VIDEO_BITRATE}"k
	-bufsize "$((FFMPEG_VIDEO_BITRATE+FFMPEG_AUDIO_BITRATE))"k
	-maxrate "$(echo "${FFMPEG_VIDEO_BITRATE}" | awk '{print $1*1.25}')"k
	-r "${FFMPEG_VIDEO_FRAMERATE}"
	-g "$((FFMPEG_VIDEO_FRAMERATE*2))"
	-bf 3 -sc_threshold 0
	-passlogfile "${FFMPEG_PASS_PREFIX}"
	)

FFMPEG_AUDIO_FLAGS=(
	-c:a libopus -b:a "${FFMPEG_AUDIO_BITRATE}"k -ac 2 -ar 48000
	-af "aresample=async=1:min_hard_comp=0.100000:first_pts=0"
	)

FFMPEG_FORMAT_FLAGS=()

FFMPEG_FIRST_PASS=(
	ffmpeg "${FFMPEG_DEF_FLAGS[@]}" \
		"${SS[@]}" -i "${INPUTFILE}" "${TO[@]}" \
		"${FFMPEG_ALL_PASS_FLAGS[@]}" \
		-pass 1 \
		-an -f "${EXTENSION}" -y /dev/null
	)

FFMPEG_SECOND_PASS=(
	ffmpeg "${FFMPEG_DEF_FLAGS[@]}" \
		"${SS[@]}" -i "${INPUTFILE}" "${TO[@]}" \
		"${FFMPEG_ALL_PASS_FLAGS[@]}" \
		-pass 2 \
		"${FFMPEG_AUDIO_FLAGS[@]}" \
		-f "${EXTENSION}" \
		"${FFMPEG_FORMAT_FLAGS[@]}" -y "${OUTPUTFILE}"
	)

FFMPEG_CLEANUP_GLOBS=(
	"${FFMPEG_PASS_PREFIX}"'*.log*'
	'x264_lookahead.*'
	)

FFMPEG_PID=''

function format_cmdline {
	printf '\e[94m%s\e[0m' "${1:-}"
	shift
	for arg in "${@}" ; do
		if [[ "${arg:0:1}" == '-' ]] ; then
			printf ' \e[95m%s\e[0m' "${arg}"
		elif echo "${arg}" | grep -q '[^[:alnum:]._-]' ; then
			printf ' \e[93m'"'"'%s'"'"'\e[0m' "${arg}"
		else
			printf ' \e[93m%s\e[0m' "${arg}"
		fi
	done
}

function first_pass {
	echo -e '\e[92m=> PASS [1/2]\e[0m :' "$(format_cmdline "${FFMPEG_FIRST_PASS[@]}")"
	AV_LOG_FORCE_COLOR=1 "${FFMPEG_FIRST_PASS[@]}" &
	FFMPEG_PID="${!}"
	wait "${FFMPEG_PID}"
}

function second_pass {
	echo -e '\e[92m=> PASS [2/2]\e[0m :' "$(format_cmdline "${FFMPEG_SECOND_PASS[@]}")"
	AV_LOG_FORCE_COLOR=1 "${FFMPEG_SECOND_PASS[@]}" &
	FFMPEG_PID="${!}"
	wait "${FFMPEG_PID}"
}

function cleanup_pass_files {
	mapfile -t ffmpeg_cleanup_files < \
		<(eval ls -1 "${FFMPEG_CLEANUP_GLOBS[@]}" 2>/dev/null || true)
	[[ "${#ffmpeg_cleanup_files[@]}" -eq 0 ]] && return 0
	echo -e '\e[92m=> CLEANUP\e[0m :' "$(format_cmdline '' "${ffmpeg_cleanup_files[@]}")"
	rm -f "${ffmpeg_cleanup_files[@]}"
}

function signal_handler {
	kill -15 "${FFMPEG_PID}" || true
	if wait "${FFMPEG_PID}" ; then : ; fi
	status="${?}"
	cleanup_pass_files || true
	exit "${status}"
}

trap signal_handler HUP INT QUIT TERM

first_pass
second_pass
cleanup_pass_files
